# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')


#########################
# Actual Code starts here


## Constants

CE0 = 2    # [mol/L]
k   = 0.3  # [1/s]


## Time

t = np.linspace(0,10,100)

## Batch

X = 1 - np.exp(-k * t)
plt.plot(t, X, '-', linewidth=1, label='Batch', color='red')



## CSTR

X = k * t / (1 + k * t)
plt.plot(t, X, '-', linewidth=1, label='CSTR')



## PFR

X = 1 - np.exp(-k * t)
plt.plot(t, X, ':', linewidth=1, label='PFR', color='black')




#########################


# Axis labels
plt.xlabel(r'$t$ / s (Batch) or $\tau$ / s (CSTR, PFR)', fontsize=16)
plt.ylabel(r'$X$', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.legend()
plt.tight_layout()
plt.savefig('../plots/ex1_conv.pdf')
