\input{/home/alexander_schoch/.templates/preamble.tex}
\input{/home/alexander_schoch/.templates/chemicalMacros.tex}
\input{/home/alexander_schoch/.templates/lstsetup.tex}

\autor{Alexander Schoch}
\titel{Homogeneous Reaction Design - Exercise 1}


\begin{document}
  \maketitle

  \section{Exercise 1}
  
  First, we formulate the general mass balance 

  \begin{equation}
    \label{eq:mass_balance}
    \frac{\dd N_\text{E}}{\dd t} = F_\text{E}^\text{in} - F_\text{E}^\text{out} + \int_V r_\text{E} \dd V
  \end{equation}

  and the reaction

  \begin{equation}
    \label{eq:reaction}
    \schemestart
      E \arrow{->[$k$]}[,0.5] R
    \schemestop.
  \end{equation}

  Additionally, we need to define the conversion $X$ as 

  \begin{equation}
    X = \frac{N^0 - N}{N^0} \Longleftrightarrow X = \frac{F^\text{in} - F^\text{out}}{F^\text{in}},
  \end{equation}
  
  which can be expressed as 

  \begin{equation}
    \label{eq:conversion}
    N(t) = N^0(1 - X) \Longleftrightarrow X = F^\text{out}(t) = F\text{in}(t)(1 - X)
  \end{equation}

  with a little bit of rearranging.


  \begin{figure}[H]
    \centering
    \begin{minipage}{0.33\linewidth}
      \caption{After deriving the dependence on (residence) time in the next subsections, $C_\text{E}$ can be plotted. It is visible that the conversion is lower for the CSTR compared to PFR and Batch for the same reactor volume. The source code for this plot can be found in the Appendix (sec. \ref{sec:appendix})}
      \label{fig:conc}
    \end{minipage}
    \begin{minipage}{0.66\linewidth}
      \includegraphics[width=\linewidth]{../plots/ex1_conc.pdf}
    \end{minipage}
  \end{figure}


  \begin{figure}[H]
    \centering
   \begin{minipage}{0.66\linewidth}
      \includegraphics[width=\linewidth]{../plots/ex1_conv.pdf}
    \end{minipage}  
    \begin{minipage}{0.33\linewidth}
      \caption{insert}
      \label{fig:conv}
    \end{minipage}
   
  \end{figure}




  \subsection{Batch}

  In the case of a batch reactor, as there are no flows into/from the reactor,  $F_\text{E}^\text{in} = F_\text{E}^\text{in} = 0$. Therefore, the mass balance reduces to 

  \begin{equation}
    \frac{\dd N_\text{E}}{\dd t} = \int_V r_\text{E} \dd V,
  \end{equation}

  whereat a batch normally has constant volume. This simplifies the mass balance even further to

  \begin{equation}
    \label{eq:mass_balance_batch}
    \frac{\dd N_\text{E}}{\dd t} = r_\text{E} V.
  \end{equation}

  As \ref{eq:reaction} is an elementary reaction, the reaction rate $r_\text{E}$ can be written as

  \begin{equation}
    r_\text{E} = -k C_\text{E},
  \end{equation}

  which can be plugged into eq. \ref{eq:mass_balance_batch}, resulting in 

  \begin{equation}
    \frac{\dd C_\text{E}}{\dd t} = -kC_\text{E}
  \end{equation}

  using $N_\text{E} = C_\text{E} V$. This ordinary differential equation can be solved using separation of variables and plugging in the initial condition $C_\text{E}(t = 0) = C_\text{E}^0 = \SI{2}{\mol\per\liter}$, leaving us with 

  \begin{mdframed}
    \begin{equation}
      \label{eq:conc_batch}
      C_\text{E} (t) = C_\text{E}^0 e^{-kt},
    \end{equation}
  \end{mdframed}

  whose plot can be seen in fig. \ref{fig:conc}.\par\bigskip

  Deriving eq. \ref{eq:conversion} with respect to $t$, 
  
  \begin{equation}
    \frac{\dd N_\text{E}}{\dd t} = -N_\text{E}^0\frac{\dd X}{\dd t}
  \end{equation}

  results. Replacing the accumulation term using eq. \ref{eq:mass_balance_batch}, the differential equation for the conversion can be found:

  \begin{equation}
    \frac{\dd X}{\dd t} = -\frac{V r_\text{E}}{N_\text{E}^0}
  \end{equation}


  Now, $r_\text{E} = -kC_\text{E}$ and $V = N_\text{E}C_\text{E}$ can be plugged in, leaving

  \begin{equation}
    \frac{\dd X}{\dd t} = -k\frac{C_\text{E}}{C_\text{E}^0}.
  \end{equation}
  
  As a last step, as eq. \ref{eq:conc_batch} describes the depenence of the concentration on time, the ordinary differential equation can be solved via 

  \begin{equation}
    \int \dd X(t) = -\frac{k}{C_\text{E}^0} \int C_\text{E}^0 e^{-kt} dt \\
  \end{equation}

  and $X$ can be expressed as 

  \begin{mdframed}
    \begin{equation}
      X(t) = 1 - e^{-kt}
    \end{equation}
  \end{mdframed}

  using the initial condition $X(0) = 0$.

  
  

  







  \subsection{CSTR}

  Starting from the mass balance \ref{eq:mass_balance}, we can assume that the CSTR reactor is run in \quotes{steady state}, meaning that the amount of E inside the reactor is constant and thus not dependent on time. The mass balance therefore reduces to 

  \begin{equation}
    0 = F_\text{E}^\text{in} - F_\text{E}^\text{out} + \int_V r_\text{E} \dd V.
  \end{equation}

  Assuming that the reactor volume is constant, the flow $F_\text{E}$ can be expressed $F_\text{E} = QC_\text{E}$ with the volumetric flow $Q$. The mass balance can thus be written as

  \begin{equation}
    Q(C_\text{E}^\text{in} - C_\text{E}^\text{out}) + r_\text{E}V = 0.
  \end{equation}

  Deviding everything by $Q$ and using $V = Q\tau$ with the average residence time $\tau$, the concentration $C_\text{E}$ can be expressed as 

  \begin{equation}
    C_\text{E} = r_\text{E}\tau + C_\text{E}^\text{in}.
  \end{equation}

  With $r_\text{E} = -kC_\text{E}$ and rearranging the equation,

  \begin{equation}
    C_\text{E}(\tau) = \frac{C_\text{E}^0}{1 + k \tau}
  \end{equation}

  is the dependence of $C_\text{E}$ on $\tau$. This function can also be seen in fig. \ref{fig:conc}.




  \subsection{PFR}

   



  %Plugging in $r_\text{E} = -kC_\text{E}$ 


  \section{Exercise 2}

  \section{Appendix}\label{sec:appendix}

  \subsection{Code}

  \includecode{../py/ex1_conc.py}{Code for fig. \ref{fig:conc} using \texttt{python}}{py}
  \includecode{../py/ex1_conv.py}{Code for fig. \ref{fig:conv} using \texttt{python}}{py}
  

\end{document}
